import React from "react";
import { withNavBars } from "./../../HOCs";
import { Home } from "./home";

class HomeParent extends React.Component {
  render() {
    return <Home />;
  }
}

export default withNavBars(HomeParent);
