import { makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { SideNavBar, TopNavBar } from "../components";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  content: {
    height: "100%",
    overflow: "auto",
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 56,
    },
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 0,
    },
  },
  topNavbar: {
    width: "100%",
  },
  sideNavbar: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
}));

const withNavBars = (Component) => (props) => {
  const classes = useStyles({ props });

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={0} lg={2}>
          <div className={classes.sideNavbar}>
            <SideNavBar />
          </div>
        </Grid>
        <Grid item xs={12} lg={10}>
          <div className={classes.topNavbar}>
            <TopNavBar />
          </div>
          <div className={classes.content}>
            <Component {...props}>{props.children}</Component>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default withNavBars;
