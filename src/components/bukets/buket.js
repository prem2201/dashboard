import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import React from "react";
import Card2 from "./card2";
import Card1 from "./cards1";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 25,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  color: {
    color: "white",
    marginTop: 2,
  },
  buket: {
    marginLeft: 15,
  },

  icons: {
    color: "black",
    fontSize: 24,
  },
  border: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    padding: 5,
    marginTop: 5,
    marginLeft: 4,
  },
  box: {
    width: "100%",
    padding: 15,
    marginTop: -20,
  },
  button: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    paddingTop: 6,
    paddingBottom: 6,
    paddingRight: 10,
    paddingLeft: 10,
  },
}));
const BuketTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    h6: {
      fontSize: 15,
    },
    subtitle1: {
      fontSize: 12,
      fontWeight: "bold",
    },
  },
});

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={BuketTheme}>
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12} md={6} lg={12}>
            <Card1 />
          </Grid>
          <Grid item xs={12} md={6} lg={12}>
            <Card2 />
          </Grid>
        </Grid>
      </div>
    </ThemeProvider>
  );
}
