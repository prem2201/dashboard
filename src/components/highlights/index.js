import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import Highlightcard1 from "./highlightcard1";
import Highlightcard2 from "./highlightscard2";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
  },
  circle: {
    backgroundColor: "black",
    width: "1.4rem",
    height: "1.4rem",
    marginLeft: 6,
  },
  color: {
    color: "white",
    marginTop: 2,
  },
  buket: {
    marginLeft: 15,
  },
}));
const BuketTheme2 = createTheme({
  typography: {
    h6: {
      fontSize: 17,
    },
    subtitle1: {
      fontSize: 12,
      fontWeight: "bold",
    },
  },
});

export default function Highlights() {
  const classes = useStyles();
  return (
    <div>
      <Grid container>
        <Grid item xs={12}>
          <ThemeProvider theme={BuketTheme2}>
            <Grid container>
              <Grid item xs={12}>
                <Box display="flex" flexDirection="row">
                  <Box className={classes.buket} m={1}>
                    <Typography variant="subtitle1">Highlights</Typography>
                  </Box>
                  <Box borderRadius="50%" className={classes.circle} m={1}>
                    <Typography variant="subtitle1" className={classes.color}>
                      02
                    </Typography>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </ThemeProvider>
          <Box p={1}>
            <Highlightcard1 />
          </Box>
          <Box p={1}>
            <Highlightcard2 />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
