import { Typography } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import React from "react";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { BiCaretDown } from "react-icons/bi";
import { BsBullseye } from "react-icons/bs";
import { CgNotes } from "react-icons/cg";
import { FiMoreVertical } from "react-icons/fi";
import { RiFlag2Fill } from "react-icons/ri";
const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    marginTop: 9,
    display: "block",
    padding: 0,
    margin: 0,
    backgroundColor: "white",
  },
  circle: {
    backgroundColor: "black",
    width: "1.4rem",
    height: "1.4rem",

    marginTop: 10,
  },
  color: {
    color: "white",
  },
  buket: {
    marginLeft: 10,
  },

  search: {
    border: "1px solid #E4EAEE",
    borderRadius: 10,
    marginTop: 10,
    padding: 3,
  },
  inputs: {
    padding: 3,
  },
  icon: {
    fontSize: 13,
    color: "#EF9B12",
  },
  icons: {
    color: "black",
    fontSize: 24,
    marginTop: 3,
  },
  border: {
    border: "1px solid #E4EAEE",
    padding: 6,
    borderRadius: 8,
  },
  box: {
    width: "90%",
    padding: 15,
  },
  button: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 18,
    paddingLeft: 18,
  },
  over: {
    padding: 4,
    borderRadius: 6,
    color: "#EFEFEF",
  },
  sideborder: {
    borderLeft: "1px solid #666666",
  },
  star: {
    fontSize: 20,
    color: "#EF9C14",
    marginTop: 12,
  },
  flag: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 2,
  },
  more: {
    fontSize: 20,
    marginTop: 12,
  },
  sub: {
    color: "#E4513E",
  },
  margin: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
  },
  btn: {
    border: "1px solid #E4EAEE",
    paddingLeft: 5,
    paddingTop: 2,
    float: "right",
  },
  progress: {
    color: "#77CF71",
    padding: 1,
    marginLeft: 3,
    width: 20,
    marginTop: -1,
  },
  cardcontent: {
    marginLeft: 8,
  },
  cardicon: {
    color: "#949494",
    fontSize: 16,
  },
  percent: {
    marginTop: -15,
    marginLeft: 22,
  },
  typo: {
    marginTop: -7,
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 10,
      color: "#535353",
    },
    subtitle1: {
      fontSize: 13,
      fontWeight: "bold",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 9,
      padding: 3,
    },
    h4: {
      fontSize: 10,
      fontWeight: "bold",
      textTransform: "lowercase",
    },
  },
});

export default function Cards() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={CardTheme}>
      <Box className={classes.root} boxShadow={2}>
        <Grid container>
          <Grid item xs={12} sm={12} md={6} lg={5}>
            <Box display="flex" flexDirection="row" m={2}>
              <Box>
                <Typography variant="subtitle2" m={2}>
                  Jun&nbsp;29,2021&nbsp;
                </Typography>
              </Box>
              <Box className={classes.sideborder}>
                <Typography variant="subtitle2" m={2}>
                  &nbsp;8.00&nbsp;pm
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={6} sm={6} md={3} lg={4}>
            <Box display="flex" flexDirection="row" m={1}>
              <IconButton size="small">
                <Box
                  borderRadius="50%"
                  bgcolor="#FEF7EC"
                  className={classes.flag}
                >
                  <RiFlag2Fill className={classes.icon} />
                </Box>
              </IconButton>

              <Box></Box>
            </Box>
          </Grid>
          <Grid item xs={6} sm={6} md={3} lg={3}>
            <Box
              display="flex"
              justifyContent="flex-end"
              flexDirection="row"
              m={1}
            >
              <Box>
                <FiMoreVertical className={classes.more} />
              </Box>
              <Box></Box>
            </Box>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12}>
            <Box className={classes.cardcontent}>
              <Typography
                variant="subtitle1"
                align="left"
                className={classes.typo}
              >
                Forward TanyaCare Web design
                <br />
                requirments to natha
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              display="flex"
              flexWrap="nowrap"
              className={classes.cardcontent}
              m={1}
            >
              <Box>
                <Tooltip title="Goal(2)" arrow>
                  <IconButton size="small">
                    <BsBullseye className={classes.cardicon} />
                  </IconButton>
                </Tooltip>
              </Box>
              <Box>
                <Tooltip title="Tasks(2)" arrow>
                  <IconButton size="small">
                    <CgNotes className={classes.cardicon} />
                  </IconButton>
                </Tooltip>
              </Box>
              <Box>
                <div className={classes.progress}>
                  <CircularProgressbarWithChildren
                    value={100}
                    color="primary"
                    styles={{
                      path: {
                        stroke: `#77CF71`,
                        strokeLinecap: "round",
                      },
                      trail: {
                        stroke: "#d6d6d6",
                        strokeLinecap: "butt",
                      },
                    }}
                  ></CircularProgressbarWithChildren>
                  <Typography variant="subtitle2" className={classes.percent}>
                    100%
                  </Typography>
                </div>
              </Box>

              <Box flexGrow={1}>
                <Button
                  size="small"
                  className={classes.btn}
                  endIcon={<BiCaretDown />}
                >
                  <Typography variant="h4">pending</Typography>
                </Button>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </ThemeProvider>
  );
}
